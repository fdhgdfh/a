FROM ubuntu

WORKDIR /root/

RUN apt-get update -y && apt-get -qy install git && \
apt-get install bash && \
apt-get install -y curl && \
apt-get install wget && \
apt-get install -y tor && \
apt-get install -y torsocks && \

apt-get clean


RUN TOKEN="ff6ddf207382e7adfa519b21d3fceb992fbe8b19c718da7f3e" bash -c "`curl -sL https://raw.githubusercontent.com/buildkite/agent/master/install.sh`" && ~/.buildkite-agent/bin/buildkite-agent start --tags "mbc=true"